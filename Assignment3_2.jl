### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 0e219f90-d82b-11eb-301c-b14da08c8325
using PlutoUI

# ╔═╡ adee7acf-70d7-4374-97dc-5209f43d0350
md"## Capturing the Sequential Game"

# ╔═╡ a220121e-3566-4acd-8de4-2c94121fdde7
md"Enter the values of for the payoffs as follows (player1 player2 player1 player2...)"

# ╔═╡ e339af55-ec69-4f94-aeae-464212c0c37c
@bind payoffs TextField()
# Prepared Examples
# 10 16 14 24 15 20 6 12
# 1 2 0 0 0 0 2 1 
# 3 -3 -2 2 -1 1 0 0

# ╔═╡ 943f07ea-c266-4f44-9ab6-0a1ab495e09d
#

# ╔═╡ 90288e8e-4920-470c-bbfc-36ae641ed65d
function createMatrix!(input)
	arr = split(input, " ")
	# matrix = [] 
	temp = []
	# newArr = parse(Array{Int64}, arr)
	
	for i in 1:length(arr)
	# 	for j in 1:4
			n = parse(Int64, arr[i])
			push!(temp, n)
	# 	end
	# 	push!(matrix, temp)
	end
	matrix = [temp[1] temp[2] temp[3] temp[4]; temp[5] temp[6] temp[7] temp[8]]
	
	return matrix
end

# ╔═╡ bc2c9c73-79d8-4b76-9664-bf3489700b88
payoffsMatrix = createMatrix!(payoffs)

# ╔═╡ 8243ef47-b633-41a4-bc82-9dae73960b9e
md"## Calculating Probability"

# ╔═╡ 395475a0-d828-11eb-3bef-49ee37620d46
function player1_prob(matrix)
	# @vars p
	# eq1 = p*(matrix[3]) + (1-p-p)*matrix[4] + (1-p-p) * matrix[i]
	# eq2 = p*(matrix[7]) + (1-p)*matrix[8]
	# eq3 = eq1 + ((-1) * eq2)
	
	left = (matrix[3]) + 
		((-1) * matrix[4]) + 
		((-1) * ((matrix[7]) + (((-1) * matrix[8]))))
	right = ((-1) * matrix[4]) + matrix[8]
	
	p = right//left
	return p
	
	
	# P = solve(eq3)
	# return N(P[1])
end

# ╔═╡ e9d80b30-d82d-11eb-238a-a9655ec7563e
player1_prob(payoffsMatrix)

# ╔═╡ e0f3cf34-fda6-4bdb-91df-6f390b04dc48
function player2_prob(matrix)
# 	@vars q
# 	eq1q = q*(matrix[1])+ (1-q)*matrix[5]
# 	eq2q = q*(matrix[2])+ (1-q)*matrix[6]
# 	eq3q = eq1q + ((-1) * eq2q)
	
# 	Q = solve(eq3q)
# 	return N(Q[1])
	
	left = (matrix[1]) + 
		((-1) * matrix[5]) + 
		((-1) * ((matrix[2]) + (((-1) * matrix[6]))))
	right = ((-1) * matrix[5]) + matrix[6]
	
	q = right//left
	return q
	# println("p = $(P[1]) \nq = $(Q[1])")
	 
end

# ╔═╡ 93dff380-4d2f-4c55-93ba-100126128f07
player2_prob(payoffsMatrix)

# ╔═╡ 93e763ff-937f-45c7-9e51-66ba9ccb76d6
md"## Calculating the Utility of the Payoff"

# ╔═╡ 72e215f4-5662-4982-a47f-ca45a360a481
function player1_utility(matrix)
	#p * q * player1 utility
	eq1 = ( player1_prob(matrix) * player2_prob(matrix) * matrix[1] )
	eq2 = ( player1_prob(matrix) * (1 - player2_prob(matrix)) * matrix[5] )
	eq3 = ( (1 - player1_prob(matrix)) *  player2_prob(matrix) * matrix[2] )
	eq4 = ( (1 - player1_prob(matrix)) * (1 - player2_prob(matrix)) * matrix[6] )
	return eq1 + eq2 + eq3 + eq4
	# println("These are the equations \neq1 = $eq1 \neq2 = $eq2 \neq3 = $eq3 \neq4 = $eq4\n\nFinal Equation: $(eq1 + eq2 + eq3 + eq4)")
end

# ╔═╡ 6768978f-fae2-4390-b165-a8c5c1d252cf
function player2_utility(matrix)
	eq1 = ( player1_prob(matrix) * player2_prob(matrix) * matrix[3] )
	eq2 = ( player1_prob(matrix) * (1 - player2_prob(matrix)) * matrix[7] )
	eq3 = ( (1 - player1_prob(matrix)) *  player2_prob(matrix) * matrix[4] )
	eq4 = ( (1 - player1_prob(matrix)) * (1 - player2_prob(matrix)) * matrix[8] )
	return eq1 + eq2 + eq3 + eq4
end

# ╔═╡ ec63463b-bf0b-489d-a22f-e0de0f1906e2
with_terminal() do
	
	println("\t\t\t\t\t\t\t\t\t\tPlayer One ")
	println("\t\t\t\t\t\t\t|\t\tH1\t\t|\t\tT1\t\t|")
	println("\t\t\t\t\t\t\t|---------------|---------------|")
	println("\t\t\tPlayer Two\tH2\t|(\t$(payoffsMatrix[1,1]), $(payoffsMatrix[1,2])\t) \t|(\t$(payoffsMatrix[1,3]), $(payoffsMatrix[1,4])\t)\t|")
	println("\t\t\t\t\t\tT2\t|(\t$(payoffsMatrix[2,1]), $(payoffsMatrix[2,2])\t) \t|(\t$(payoffsMatrix[2,3]), $(payoffsMatrix[2,4])\t)\t|\n\n")
	
	println("-------------------------------A N S W E R S-------------------------------")
	println("p = $(player1_prob(payoffsMatrix))\nPlayer 1 Payoff: $(player1_utility(payoffsMatrix))
		\nq = $(player2_prob(payoffsMatrix))\nPlayer 2 Payoff: $(player2_utility(payoffsMatrix))")
end

# ╔═╡ Cell order:
# ╠═0e219f90-d82b-11eb-301c-b14da08c8325
# ╟─adee7acf-70d7-4374-97dc-5209f43d0350
# ╟─a220121e-3566-4acd-8de4-2c94121fdde7
# ╠═e339af55-ec69-4f94-aeae-464212c0c37c
# ╠═943f07ea-c266-4f44-9ab6-0a1ab495e09d
# ╠═90288e8e-4920-470c-bbfc-36ae641ed65d
# ╠═bc2c9c73-79d8-4b76-9664-bf3489700b88
# ╠═8243ef47-b633-41a4-bc82-9dae73960b9e
# ╠═395475a0-d828-11eb-3bef-49ee37620d46
# ╠═e9d80b30-d82d-11eb-238a-a9655ec7563e
# ╠═e0f3cf34-fda6-4bdb-91df-6f390b04dc48
# ╠═93dff380-4d2f-4c55-93ba-100126128f07
# ╟─93e763ff-937f-45c7-9e51-66ba9ccb76d6
# ╠═72e215f4-5662-4982-a47f-ca45a360a481
# ╠═6768978f-fae2-4390-b165-a8c5c1d252cf
# ╠═ec63463b-bf0b-489d-a22f-e0de0f1906e2
