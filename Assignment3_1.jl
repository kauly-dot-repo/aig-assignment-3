### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 899f4dd0-d618-11eb-3123-3fcdadeb0b88
begin 
	using Random
	using PlutoUI
end

# ╔═╡ 9f7be46c-6b60-4454-b6be-efbedc8dbfa9
md"# Assignment 3 - Problem 1"


# ╔═╡ 0045ea98-ccb6-4974-b02d-4eb38c2f850a
md"## Defining the MDP"

# ╔═╡ 918938a0-d602-11eb-278e-9344f05ae277
struct MDP
    initialState
    states
    actions
    terminal_states
    transitions
    discount::Float64
    rewards 
    
	#The constructor of MDP struct
    function MDP(initialState,actions, terminal_states, transitions, states, discount, reward)
        return new(initialState, states, actions, terminal_states, transitions, discount,reward);
    end
end


# ╔═╡ 34c5f740-d740-11eb-32da-43a1101964c9
function getBestElement(seq::Vector, fn::Function) 
    bestElement = seq[1];
    bestScore = fn(bestElement);
    for element in seq
        elementScore = fn(element);
        if (elementScore > bestScore)
            bestElement = element;
            bestScore = elementScore;
        end
    end
    return bestElement; #best element is the one with highest score??
end


# ╔═╡ c95d36a0-d602-11eb-26d4-033bdced1164
#Reward function for returning the reward -- for a specific mdp??
function reward(mdp, state)
    return mdp.rewards[state];
end


# ╔═╡ d1ff4be0-d602-11eb-35c5-4d9c799e4639
#transion model to return the transitionModel of the state with the given action
function transitionModel(mdp, state, action)
    return [mdp.transitions[state][action]]; #RETURNS the probability of action and 
											 #the state that action leads to
end



# ╔═╡ 5b52a0bb-63ee-4683-9c1e-fc3e2ba56bb2
# transitionModel(mdp, "S1", "NORTH")

# ╔═╡ e3c131e0-d602-11eb-2561-7b9a2f0b4738
#actions function to return the actions
function actions(mdp, state)
        return mdp.actions;
end

# ╔═╡ 4751ae85-b8a1-4e40-bb54-2d91a98deb9e
function policyImprovement(seq::Vector, fn::Function)
     #getting the maximum action
	 local action = getBestElement(seq, fn);
     #compares policies and picks the best
	return action
end 

# ╔═╡ f4447d60-d602-11eb-1a68-5d96cd649c6a
# expected utility function returns the expected utility of doing action in state s
function expected_utility(mdp , U , state , action)
    return sum((p * U[state_prime] for (p, state_prime) in transitionModel(mdp, state, action)));
end


# ╔═╡ 09b17f1e-d605-11eb-286a-11a14e479a01
function policy_iteration(mdp) #going thru to find the best policy
	#all utilities all initialised to 0
     Utilities = Dict(collect(Pair(state, 0.0) for state in mdp.states));
	#Assign a random policy
     policy = Dict(collect(Pair(state, rand(collect(actions(mdp, state))))
                                    for state in mdp.states));
	
	
    while (true)
		#policy evaluation  
		#looping 20 times ---- WHY????
		   for i in 1:20
				for state in mdp.states
					Utilities[state] = (reward(mdp, state)
								+ (mdp.discount * sum((p * Utilities[state_prime] for (p, state_prime) in transitionModel(mdp, state, policy[state]))))
						);
				end
    	   end
       
         converge = true;
        for state in mdp.states
			
			#Policy Improvement 
			#get the best action 
            local action = policyImprovement(collect(actions(mdp, state)), (function(action) 
                  return expected_utility(mdp, Utilities, state, action);
            end));
            #checking convergence
			if (action != policy[state])
                policy[state] = action;
                converge = false;
            end
        end
		
        if (converge)
			
            return policy;
			# return Utilities
         end
     end
end

# ╔═╡ a87687a7-0a85-42a3-a8b5-91a93df1941f
transModel = Dict()

# ╔═╡ 8b1032bc-1069-42c5-8a87-0d88cfa836a9
begin
	initState = "S11"
	action_set = Set(["Up", "Down", "Left", "Right"])
	terminal = "S34"
	#transition model is defined already in the above cells
	allStates = Set(["S11","S12","S13","S14","S21","S22","S23","S24", "S31", "S32", "S33", "S34"])
	discountFactor = 1 #meaning of discount facotor?
	rewards = Dict("S11" => 0.5,"S12" => 0.5,"S13" => 0.5,"S14" => 0.5, 
				   "S21" => 0.5,"S22" => 0,"S23" => 0.5,"S24" => -1, 
				   "S31" =>0.5, "S32" => 0.5, "S33" => 0.5, "S34" => 1)
end

# ╔═╡ 4f8667b7-a1c2-4a44-9f02-fd5ae20c4f8c
# begin 
#    push!(transModel,"S1"=> Dict(["NORTH"=>(prob, "S2"), "SOUTH"=> (prob, "S3")]))
#    push!(transModel,"S2" => Dict(["NORTH"=> (prob, "S4"), "SOUTH" => (prob, "S5")]))
#    push!(transModel,"S3" => Dict(["NORTH" => (prob, "S5"), "SOUTH" =>(prob, "S6")]))
#    push!(transModel,"S4"=> Dict(["NORTH"=> (prob, "S7"), "SOUTH"=>(prob, "S7")])) 
#    push!(transModel,"S5"=> Dict(["NORTH" => (prob, "S7"), "SOUTH"=> (prob, "S7")]))
#    push!(transModel,"S6"=> Dict(["NORTH"=> (prob, "S7"), "SOUTH"=> (prob, "S7")]))
#    push!(transModel,"S7"=> Dict(["NORTH"=> (prob, "S7"), "SOUTH"=> (prob, "S7")]))
# end  

# ╔═╡ 71f071e9-debc-4b0d-a738-c54fc3976f31
empty!(transModel)

# ╔═╡ 4e959575-56db-4299-a212-17db51effecc
# begin
# 	initState = "S1"
# 	action_set = Set(["NORTH", "SOUTH"])
# 	terminal = "S3"
# 	#transition model is defined already in the above cells
# 	allStates = Set(["S1","S2","S3","S4","S5","S6","S7"])
# 	discountFactor = 1 #meaning of discount facotor?
# 	rewards = Dict("S1" => 0 , "S2" => 2, "S3" => 1, "S6" => -0.5 , "S5" => 1, "S4" => -2 ,"S7" => 0)
# end


# ╔═╡ 677c906b-62c0-46cc-9965-ab80420da5b1
prob = 1/length(action_set)

# ╔═╡ ce65fc35-7353-47cd-882d-5b3d53b8d42b
begin
#grid model - (["Up", "Down", "Left", "Right"])
	 push!(transModel, "S11" => Dict(["Up" => (prob, "S21"), "Down" => (0.0, "S11"), 								 "Left" => (0.0, "S11"), "Right" => (prob, "S12")]))
	 push!(transModel, "S12" => Dict(["Up" => (0.0, "S12"), "Down" => (0.0, "S12"),
								"Left" => (prob, "S11"), "Right" => (prob, "S13")]))
	 push!(transModel, "S13" => Dict(["Up" => (prob, "S23"), "Down" => (0.0, "S13"),
								"Left" => (prob, "S12"), "Right" => (prob, "S14")]))
	 push!(transModel, "S14" => Dict([ "Up" => (prob, "S24"), "Down" => (0.0, "S14"), 								  "Left" => (prob, "S13"), "Right" => (0.0, "S14")]))

	 push!(transModel, "S21" => Dict(["Up" => (prob, "S31"), "Down" => (prob, "S11"), 
								"Left" => (0.0, "S21"), "Right" => (0.0, "S21")]))
	 push!(transModel, "S22" => Dict(["Up" => (0.0, "S22"), "Down" => (0.0, "S22"),
								"Left" => (0.0, "S22"), "Right" => (0.0, "S22")]))
	 push!(transModel, "S23" => Dict(["Up" => (prob, "S33"), "Down" => (prob, "S33"), 								  "Left" => (0.0, "S22"), "Right" => (prob, "S24")]))
	 push!(transModel, "S24" => Dict(["Up" => (0.0, "S24"), "Down" => (0.0, "S24"),
								"Left" => (0.0, "S24"), "Right" => (0.0, "S24")]))

	 push!(transModel, "S31" => Dict(["Up" => (0.0, "S31"), "Down" => (prob, "S21"), 								 "Left" => (0.0, "S31"), "Right" => (prob, "S32")]))
	 push!(transModel, "S32" => Dict(["Up" => (0.0, "S32"), "Down" => (0.0, "S22"), 
								"Left" => (prob, "S31"), "Right" => (prob, "S33")]))
	 push!(transModel, "S33" => Dict(["Up" => (0.0, "S33"), "Down" => (prob, "S23"), 
								"Left" => (prob, "S32"), "Right" => (prob, "S34")]))
	 push!(transModel, "S34" => Dict(["Up" => (0.0, "S34"), "Down" => (0.0, "S34"),
								"Left" => (0.0, "S34"), "Right" => (0.0, "S34")]))
end

# ╔═╡ 1730eb60-d603-11eb-0a1a-71225e1024e4
mdp = MDP(initState, action_set, terminal, transModel, allStates, discountFactor, rewards);

# mdp = MDP("S1", Set(["NORTH", "SOUTH"]), Set(["S3"]), transModel, Set(["S1","S2","S3","S4","S5","S6","S7"]), discountFactor,rewards);


# ╔═╡ 977e473d-98f2-4555-abff-c8a11c913f40
policy_iteration(mdp)

# ╔═╡ e7f839ee-d604-11eb-2d96-8300ae218651
optimal = policy_iteration(mdp)

# ╔═╡ 690348e4-1650-42da-b439-3d23f63e986b
# with_terminal() do
# 	println("Optimal Policy:\n", optimal)
# end

# ╔═╡ Cell order:
# ╟─9f7be46c-6b60-4454-b6be-efbedc8dbfa9
# ╠═899f4dd0-d618-11eb-3123-3fcdadeb0b88
# ╟─0045ea98-ccb6-4974-b02d-4eb38c2f850a
# ╠═918938a0-d602-11eb-278e-9344f05ae277
# ╠═34c5f740-d740-11eb-32da-43a1101964c9
# ╠═c95d36a0-d602-11eb-26d4-033bdced1164
# ╠═d1ff4be0-d602-11eb-35c5-4d9c799e4639
# ╠═5b52a0bb-63ee-4683-9c1e-fc3e2ba56bb2
# ╠═e3c131e0-d602-11eb-2561-7b9a2f0b4738
# ╠═4751ae85-b8a1-4e40-bb54-2d91a98deb9e
# ╠═09b17f1e-d605-11eb-286a-11a14e479a01
# ╠═977e473d-98f2-4555-abff-c8a11c913f40
# ╠═f4447d60-d602-11eb-1a68-5d96cd649c6a
# ╠═a87687a7-0a85-42a3-a8b5-91a93df1941f
# ╠═8b1032bc-1069-42c5-8a87-0d88cfa836a9
# ╠═ce65fc35-7353-47cd-882d-5b3d53b8d42b
# ╠═4f8667b7-a1c2-4a44-9f02-fd5ae20c4f8c
# ╠═71f071e9-debc-4b0d-a738-c54fc3976f31
# ╠═4e959575-56db-4299-a212-17db51effecc
# ╠═677c906b-62c0-46cc-9965-ab80420da5b1
# ╠═1730eb60-d603-11eb-0a1a-71225e1024e4
# ╠═e7f839ee-d604-11eb-2d96-8300ae218651
# ╠═690348e4-1650-42da-b439-3d23f63e986b
